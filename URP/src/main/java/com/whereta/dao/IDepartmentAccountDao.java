package com.whereta.dao;

import com.whereta.model.DepartmentAccount;

/**
 * @author Vincent
 * @time 2015/9/2 10:20
 */
public interface IDepartmentAccountDao {

    Integer getDepIdByAccountId(int accountId);

    int deleteByDepId(int depId);

    int deleteByAccountId(int accountId);

    int save(DepartmentAccount departmentAccount);
}
