package com.whereta.service.impl;

import com.whereta.dao.IMessageBoardDao;
import com.whereta.model.MessageBoardWithBLOBs;
import com.whereta.service.IMessageBoardService;
import com.whereta.vo.ResultVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by vincent on 15-9-24.
 */
@Service("messageBoardService")
public class MessageBoardServiceImpl implements IMessageBoardService {
    @Resource
    private IMessageBoardDao messageBoardDao;

    /**
     * 保存留言内容
     * @param userId
     * @param msg
     * @return
     */
    @Override
    public ResultVO saveMessageBoard(int userId, String msg) {
        ResultVO resultVO = new ResultVO(true);
        MessageBoardWithBLOBs messageBoard = new MessageBoardWithBLOBs();
        messageBoard.setMsg(msg);
        messageBoard.setCreateTime(new Date());
        messageBoard.setUserId(userId);
        messageBoardDao.save(messageBoard);
        resultVO.setMsg("您的留言已收到");
        return resultVO;
    }
}
