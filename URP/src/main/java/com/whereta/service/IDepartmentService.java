package com.whereta.service;

import com.whereta.vo.DepartmentCreateVO;
import com.whereta.vo.DepartmentEditVO;
import com.whereta.vo.ResultVO;

/**
 * @author Vincent
 * @time 2015/9/2 10:14
 */
public interface IDepartmentService {

    ResultVO getShowDepartments(int userId, Integer rootId,Integer checkedUserId);

    ResultVO createDepartment(DepartmentCreateVO createVO);

    ResultVO editDepartment(DepartmentEditVO createVO, int accountId);

    ResultVO deleteDep(int depId, int accountId);
}
