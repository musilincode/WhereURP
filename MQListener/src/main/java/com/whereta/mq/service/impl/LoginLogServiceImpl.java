package com.whereta.mq.service.impl;

import com.whereta.mq.dao.ILoginLogDao;
import com.whereta.mq.model.LoginLog;
import com.whereta.mq.service.ILoginLogService;
import com.whereta.mq.vo.ResultVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by vincent on 15-9-10.
 */
@Service("loginLogService")
public class LoginLogServiceImpl implements ILoginLogService {
    @Resource
    private ILoginLogDao loginLogDao;

    @Override
    public ResultVO saveLoginLog(LoginLog loginLog) {
        ResultVO resultVO = new ResultVO(true);
        int num = loginLogDao.save(loginLog);
        resultVO.setData(num);
        return resultVO;
    }
}
